#include "PGM.hpp"
#include "PPM.hpp"
#include <bits/stdc++.h>

using namespace std;

int main(int argc, char ** argv){

  Imagem imagem1;
  PGM pgm1;
  PPM ppm1;

  string nome_arquivo;

  cout << "Digite o nome e a extenção do arquivo: " << endl;
  cin >> nome_arquivo;

  imagem1.lerImagem(nome_arquivo);

  if (imagem1.getExtensao() == "PGM"){
    pgm1.lerImagem(nome_arquivo);

    pgm1.imprimeDadosImagem();

    pgm1.lerComentario();
    string menssagem;
    menssagem = pgm1.decifraMenssagem();
    cout << endl << "Menssagem descriptografada: ";
    //impede que caracteres indesejados apareçam
    for(int i = 0; i< pgm1.getTamanhoMsg(); i++){
      cout << menssagem[i];
    }
    cout << endl << endl;

    int auxiliar;
    cout << "Caso deseje fazer uma copia da imagem digite \"1\" ou \"0\" para o contrario e pressione enter." << endl;
    cin >> auxiliar;
    if (auxiliar == 1)
      pgm1.criaImagem(nome_arquivo);

  }else if (imagem1.getExtensao() == "PPM"){
    ppm1.lerImagem(nome_arquivo);

    ppm1.imprimeDadosImagem();

    ppm1.lerComentario();
    ppm1.relerDados(nome_arquivo);
    ppm1.structDados(nome_arquivo);


    string menssagem;
    menssagem = ppm1.decifraMenssagem();
    cout << endl << "Menssagem descriptografada: ";
    cout << menssagem << endl << endl;

    int auxiliar;
    cout << "Caso deseje fazer uma copia da imagem digite \"1\" ou \"0\" para o contrario e pressione enter." << endl;
    cin >> auxiliar;
    if (auxiliar == 1)
      ppm1.criaImagem(nome_arquivo);
  }else{
    cout << "O arquivo e de formato não suportado ou foi não encontrado." << endl;
  }


	return 0;
}
