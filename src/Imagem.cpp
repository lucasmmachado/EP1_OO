#include "Imagem.hpp"
#include <bits/stdc++.h>
#include <fstream>

using namespace std;

Imagem::Imagem(){
  tipo = "";
  largura = 100;
  altura = 100;
  tons = 200;
  extensao = "";
  comentario = " ";

}
Imagem::Imagem(string tipo, int largura, int altura, int tons){
  this->tipo = tipo;
  this->largura = largura;
  this->tons = tons;
}

Imagem::~Imagem(){
  cout << "Destrutor de Imagem" << endl;
  //delete dados;
}

string Imagem::getTipo(){
  return tipo;
}
int Imagem::getLargura(){
  return largura;
}
int Imagem::getAltura(){
  return altura;
}
int Imagem::getTons(){
  return tons;
}
void Imagem::setExtensao(string extensao){
  this->extensao = extensao;
}
string Imagem::getExtensao(){
  return extensao;
}
void Imagem::setComentario(string comentario){
  this->comentario = comentario;
}
string Imagem::getComentario(){
  return comentario;
}
void Imagem::setDados(char* dados){
  this->dados = dados;
}
char* Imagem::getDados(){
  return dados;
}

void Imagem::imprimeDadosImagem(){
  cout << endl;
  cout << "Tipo: " << tipo << endl;
  cout << "Comentário: " << comentario << endl;
  cout << "Largura: " << largura << "  Altura: " << altura << endl;
  cout << "Variação maxima da escala de cinza: " << tons << endl;
  cout << "Extensão da imagem: " << extensao << endl;
}

void Imagem::lerImagem(string nome_arquivo){

  ifstream file(nome_arquivo);

  if(!file.is_open())
    cout << "Erro ao ler arquivo" << endl;
  else{
    getline(file, tipo);

    if (tipo == "P2" || tipo == "P5")
      setExtensao("PGM");
    else if (tipo == "P3" || tipo == "P6")
      setExtensao("PPM");

    char aux;
    file.get(aux);
    file.unget();

    //confere se possui comentario
    if (aux == '#')
      getline(file, comentario);

    file >> altura >> largura;
    file >> tons;

    if (extensao == "PGM")
       dados = new char [altura * largura];
    else if (extensao == "PPM")
       dados = new char [altura * largura * 3];

    int cont = 0;
    while(file.get(aux)){
      dados[cont] = aux;
      cont++;
    }
    file.close();
  }
}

void Imagem::criaImagem(string nome_arquivo){
  string novo_arquivo;

  cout << endl <<"Digite o nome para a imagem que sera criada e a exetensão : " << endl;
  cin >> novo_arquivo;

  ofstream file(novo_arquivo);
  if(!file.is_open()){
    cout << "Erro ao gravar no arquivo" << endl;
  }else{
    file << tipo << endl;
    file << comentario << endl;
    file << altura << " " << largura << endl ;
    file << tons;
    file << dados << endl;
  }
  file.close();
}
