#include "PGM.hpp"
#include "Imagem.hpp"
#include <bits/stdc++.h>

using namespace std;

PGM::PGM(){
  inicio_msg = 0;
  tamanho_msg = 0;
  shift = 0;
}
PGM::PGM(int inicio_msg, int tamanho_msg, int shift){
}
PGM::~PGM(){
  cout << "Destrutor de Imagem PGM" << endl;
}

int PGM::getInicioMsg(){
  return inicio_msg;
}
int PGM::getTamanhoMsg(){
  return tamanho_msg;
}
int PGM::getShift(){
  return shift;
}
//  lê o comentario e pega os valores como caractere e transforma no formatos
//corretos e após isso seta nas variaves adequadas
void PGM::lerComentario(){
  string coment = getComentario();
  int cont = 0;
  int stop = 0;

  int tamanho_aux = 0;
  int tamanho_aux1 = 0;
  int tamanho_aux2 = 0;
  int size = coment.size();

  for (int i = 1; i < size ; i++){
    if (coment[i] != ' ' && stop == 0){
      tamanho_aux++;
    }else if(coment[i] != ' ' && stop == 1){
      tamanho_aux1++;
    }else if(coment[i] != ' ' && stop == 2){
      tamanho_aux2++;
    }else{
      stop++;
    }
  }
  char aux[tamanho_aux];
  char aux1[tamanho_aux1];
  char aux2[tamanho_aux2];
  cont = 0;
  stop = 0;
  for (int i = 1; i < size ; i++){
    if (coment[i] != ' ' && stop == 0){
      aux[cont] = coment[i];
      cont++;
    }else if(coment[i] != ' ' && stop == 1){
      aux1[cont] = coment[i];
      cont++;
    }else if(coment[i] != ' ' && stop == 2){
      aux2[cont] = coment[i];
      cont++;
    }else{
      stop++;
      cont = 0;
    }
  }

  inicio_msg = atoi (aux);
  tamanho_msg = atoi (aux1);
  shift = atoi (aux2);
}
//algoritimo para decifrar a menssagem
string PGM::decifraMenssagem(){
  char* menssagem;
  char* n_dados;
  menssagem = new char [tamanho_msg];
  n_dados = new char [getLargura() * getAltura()];

  n_dados = getDados();

  int cont = 0;
  for(int i = inicio_msg + 1; i < (inicio_msg + tamanho_msg + 1); i++){
    if(n_dados[i]>='a' && n_dados[i] <= 'z'){
      if((n_dados[i] - shift) < 'a')
        n_dados[i] += (26 - shift);
      else
        n_dados[i] -= shift;
    }
    else if(n_dados[i]>='A' && n_dados[i] <= 'Z'){
      if((n_dados[i] - shift) < 'A')
        n_dados[i] += (26 - shift);
      else
        n_dados[i] -= shift;
    }
    menssagem[cont] = n_dados[i];
    cont++;
  }

  //retirando caracteres desnecessarios
  char debug_menssagem[cont-1];
  for (int i = 0;i < cont;i++){
    debug_menssagem[i] = menssagem[i];
  }
  string menssagem_real = {};
  menssagem_real = debug_menssagem;

  return menssagem_real;
}
