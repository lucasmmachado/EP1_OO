#include "PPM.hpp"
#include "Imagem.hpp"
#include <bits/stdc++.h>
#include <fstream>

using namespace std;

PPM::PPM(){
  inicio_msg = 0;
  tamanho_msg = 0;
  chave = "";
  alfabeto = "";
}
PPM::PPM(int inicio_msg, int tamanho_msg, string chave){
}
PPM::~PPM(){
  cout << "Destrutor de Imagem PPM" << endl;
  //delete n_dados;
}

int PPM::getInicioMsg(){
  return inicio_msg;
}
int PPM::getTamanhoMsg(){
  return tamanho_msg;
}
string PPM::getChave(){
  return chave;
}
void PPM::imprimeDadosImagem(){
  cout << endl;
  cout << "Tipo: " << getTipo() << endl;
  cout << "Largura:" << getLargura() << "  Altura:" << getAltura() << endl;
  cout << "Variação maxima das cores : " << getTons() << endl;
  cout << "Extensão da imagem: " << getExtensao() << endl;
}
//  lê o comentario e pega os valores como caractere e transforma no formatos
//corretos e após isso seta nas variaves adequadas
void PPM::lerComentario(){
  string coment = getComentario();
  int cont = 0;
  int stop = 0;

  int tamanho_aux = 0;
  int tamanho_aux1 = 0;
  int tamanho_aux2 = 0;
  int size = coment.size();

  for (int i = 1; i < size ; i++){
    if (coment[i] != ' ' && stop == 0){
      tamanho_aux++;
    }else if(coment[i] != ' ' && stop == 1){
      tamanho_aux1++;
    }else if(coment[i] != ' ' && stop == 2){
      tamanho_aux2++;
    }else{
      stop++;
    }
  }

  char aux[tamanho_aux];
  char aux1[tamanho_aux1];
  char aux2[tamanho_aux2];
  cont = 0;
  stop = 0;
  for (int i = 1; i < size ; i++){
    if (coment[i] != ' ' && stop == 0){
      aux[cont] = coment[i];
      cont++;
    }else if(coment[i] != ' ' && stop == 1){
      aux1[cont] = coment[i];
      cont++;
    }else if(coment[i] != ' ' && stop == 2){
      aux2[cont] = coment[i];
      cont++;
    }else{
      stop++;
      cont = 0;
    }
  }
  inicio_msg = atoi (aux);
  tamanho_msg = atoi (aux1);
  chave = aux2;
}
void PPM::setAlfabeto(){
  string alfabeto = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

  char novo_alfabeto[25];
  int size = chave.size();
  for(int i = 0; i < size; i++){
    //transforma letras minuscualas em maiuscula
    if((chave[i] >= 95) && (chave[i] <= 123))
      chave[i] -= 32;
    novo_alfabeto[i] = chave[i];
  }

  int aux = 0;
  int cont = chave.size();

  //cria alfabeto com a plavra chave
  for(int i = 0; i < 26; i++){
    for(int j = 0; j < size; j++){
      if (alfabeto[i] == chave[j])
        aux++;
    }
    if (aux == 0){
      novo_alfabeto[cont] = alfabeto[i];
      cont++;
    }
    aux=0;
  }

  string n_alfabeto = novo_alfabeto;
  this->alfabeto = n_alfabeto;
}

void PPM::relerDados(string nome_arquivo){

  ifstream file(nome_arquivo);
  string armazena;

  if(!file.is_open())
  cout << "Erro ao ler arquivo" << endl;
  else{
    getline(file, armazena);

    char aux;
    n_dados = new unsigned char [getAltura() * getLargura() * 3];
    getline(file, armazena);
    int buffer;
    file >> buffer >> buffer;
    file >> buffer;

    int cont = 0;
    while(file.get(aux)){
      n_dados[cont] = aux;
      cont++;
    }
    file.close();
  }

}


void PPM::structDados(string nome_arquivo){

  ifstream file(nome_arquivo);
  string armazena;

  if(!file.is_open())
    cout << "Erro ao ler arquivo" << endl;
  else{
    getline(file, armazena);
    getline(file, armazena);
    getline(file, armazena);
    getline(file, armazena);

    char aux;
    int size = getAltura() * getLargura();
    for (int i = 0; i < size; i++){
      Pixel temp;
      file.get(aux);
      temp.r = aux;
      file.get(aux);
      temp.g = aux;
      file.get(aux);
      temp.b = aux;
      v_pixel.push_back(temp);
    }
    file.close();
  }
}

void PPM::criaImagem(string nome_arquivo){
  //cria imagem com unsigned char
  string novo_arquivo;
  cout << endl <<"Digite o nome da imagem que sera criada e exetensão : " << endl;
  cin >> novo_arquivo;

  ofstream file(novo_arquivo);
  if(!file.is_open()){
    cout << "Erro ao gravar no arquivo" << endl;
  }else{
    file << getTipo() << endl;
    file << getComentario() << endl;
    file << getLargura() << " " << getAltura() << endl ;
    file << getTons();
    file << n_dados << endl;
  }
  file.close();
}
//menssagem de acordo com as posiçoes das letras no novo alfabeto
int *PPM::valorFrase(){
  int *menssagem;
  menssagem = new int [tamanho_msg];

  int cont = 0;
  int inicio;
  inicio = inicio_msg * 3;

  for(int i = inicio; i < (inicio + tamanho_msg) ; i++){
    menssagem[cont] = v_pixel[i].r%10 + v_pixel[i].g%10 + v_pixel[i].b%10;
    cont++;
  }

  return menssagem;
}
//algoritimo para decifrar a menssagem
string PPM::decifraMenssagem(){
  setAlfabeto();

  int *menssagem;
  menssagem = new int [tamanho_msg];
  menssagem = valorFrase();

  //descriptografa apartir do novo alfabeto
  char n_menssagem[tamanho_msg - 1];
  for (int i = 0; i < tamanho_msg ; i++){
    if (menssagem[i] == 0)
      n_menssagem[i] = ' ';
    else if ((menssagem[i] - 1) < 26)
      n_menssagem[i] = alfabeto[(menssagem[i] - 1)];
    else if ((menssagem[i] - 1) > 26)
    n_menssagem[i] = alfabeto[(menssagem[i] - 27)];
  }
  string menssagem_real;
  menssagem_real = n_menssagem;
  return menssagem_real;
}
