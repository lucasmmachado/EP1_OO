#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <bits/stdc++.h>

using namespace std;

class Imagem{

private:
  string tipo;
  int largura;
  int altura;
  int tons;
  string extensao;
  string comentario;
  char *dados;

public:
  Imagem();
  Imagem(string tipo, int largura, int altura, int tons);

  ~Imagem();

  void setTipo(string tipo);
  string getTipo();
  int getLargura();
  int getAltura();
  int getTons();
  void setExtensao(string extensao);
  string getExtensao();
  void setComentario(string comentario);
  string getComentario();
  void setDados(char* dados);
  char* getDados();

  void imprimeDadosImagem();

  void lerImagem(string nome_arquivo);

  void criaImagem(string nome_arquivo);

};

#endif
