#ifndef PPM_HPP
#define PPM_HPP

#include "Imagem.hpp"
#include <bits/stdc++.h>

using namespace std;

struct Pixel{
  int r;
  int g;
  int b;
};

class PPM : public Imagem{
private:
  int inicio_msg;
  int tamanho_msg;
  string chave;
  string alfabeto;
  vector <Pixel> v_pixel;
  unsigned char *n_dados;

public:
  PPM();
  PPM(int inicio_msg, int tamanho_msg, string chave);
  ~PPM();

  int getInicioMsg();
  int getTamanhoMsg();
  string getChave();
  void imprimeDadosImagem();

  void lerComentario();

  void relerDados();
  void setAlfabeto();

  void relerDados(string nome_arquivo);
  void structDados(string nome_arquivo);
  void criaImagem(string nome_arquivo);

  int *valorFrase();
  string decifraMenssagem();

};

#endif
