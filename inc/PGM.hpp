#ifndef PGM_HPP
#define PGM_HPP

#include "Imagem.hpp"
#include <bits/stdc++.h>

using namespace std;

class PGM : public Imagem{
private:
  int inicio_msg;
  int tamanho_msg;
  int shift;

public:
  PGM();
  PGM(int inicio_msg, int tamanho_msg, int shift);
  ~PGM();

  int getInicioMsg();
  int getTamanhoMsg();
  int getShift();

  void lerComentario();
  string decifraMenssagem();

};

#endif
