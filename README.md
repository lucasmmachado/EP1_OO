### Descriptografia de Imagens PPM e PGM


1. Coloque a imagem PPM ou PGM na pasta EP1_OO

2. Va ao terminal e encontre o caminho da pasta
3. No terminal digite os seguintes comandos:

  ```  make clean ```

  ``` make  ```

  ``` make run ```

 3.1. Aparecerá na tela:
          Digite o nome e a extenção do arquivo:

 3.2. Como entrada digite:
        nome_do_seu_arquivo.pgm ou nome_do_seu_arquivo.ppm


4. No terminal irá aparecer:

**Para imagens PGM :**

    * ------DESCRIPTOGRAFIA DA IMAGEM PGM------
    * Tipo:
    * Comentário:  
    * Largura :   Altura:
    * Variação maxima da escala de cinza:
    * Extensão da imagem:

    * Menssagem descriptografada:

**Para imagens PPM :**

    * ------DESCRIPTOGRAFIA DA IMAGEM PPM------
    * Tipo:
    * Comentário:  
    * Largura :   Altura:
    * Variação maxima das cores:
    * Extensão da imagem:

    * Menssagem descriptografada:


5.No terminal irá aparecer:

    * Caso deseje fazer uma copia da imagem digite "1" ou "0" para o contrario e pressione enter.


  5.1. Aparecerá na tela:

    * Digite o nome da imagem que sera criada e exetensão :
